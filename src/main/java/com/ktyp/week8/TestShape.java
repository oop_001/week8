package com.ktyp.week8;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle("rect1",10,5);
        rect1.printAreaRect();
        rect1.printPerimeterRect();
        Rectangle rect2 = new Rectangle("rect2",5,3);
        rect2.printAreaRect();
        rect2.printPerimeterRect();

        Cricle cricle1 = new Cricle("circle1",1);
        cricle1.printAreaCricle();
        cricle1.printPerimeterRectCricle();
        Cricle cricle2 = new Cricle("circle2",2);
        cricle2.printAreaCricle();
        cricle2.printPerimeterRectCricle();

        Triangle triangle = new Triangle("triangle1",5,5,6);
        triangle.printAreaTriangle();
        triangle.printPerimeterTriangle();
    }

}
