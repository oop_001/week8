package com.ktyp.week8;

public class Triangle {
    private String name;
    private double a;
    private double b;
    private double c;
    private double area;
    private double perimeter;
    private double s;
    public Triangle(String name, double a,double b,double c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public void printAreaTriangle() {
        s = (a+b+c) / 2;
        area = (int) Math.sqrt(s * (s-a) * (s-b) * (s-c));
        System.out.println("Area of triangle "+ name + " : " + area);
    }

    public void printPerimeterTriangle() {
        s = (a+b+c);
        System.out.println("Perimeter of triangle "+ name + " : " + s);
    }
}
