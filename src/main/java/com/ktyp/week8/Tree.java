package com.ktyp.week8;

public class Tree {
    private String name;
    private int x;
    private int y;
    public final static int X_MIN = 0;
    public final static int Y_MIN = 0;
    public Tree(String name,int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void printposition() {
        System.out.println(name + " x:" + x + " y:" + y);
    }
}    

