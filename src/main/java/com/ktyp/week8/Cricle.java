package com.ktyp.week8;

public class Cricle {
    private String name;
    private double r;
    private double area;
    private double perimeter;
    public Cricle(String name, double r) {
        this.name = name;
        this.r = r;
    }
    public void printAreaCricle() {
        area = Math.PI*(Math.pow(r,2));
        System.out.println("Area of Circle "+ name + " : " + area);
    }
    public void printPerimeterRectCricle() {
        perimeter = 2*(Math.PI*r);
        System.out.println("Perimeter of Cricle "+ name + " : " + perimeter);
    }
}
