package com.ktyp.week8;

public class Rectangle {
    private String name;
    private double height;
    private double width;
    private double area;
    private double perimeter;
    public Rectangle(String name, double height,double width) {
        this.name = name;
        this.height = height;
        this.width = width;
    }
    public void printAreaRect() {
        area = width * height;
        System.out.println("Area of Rectangle "+ name + " : " + area);
    }

    public void printPerimeterRect() {
        perimeter = (height * 2) + (width * 2);
        System.out.println("Perimeter of Rectangle "+ name + " : " + perimeter);
  
    }

}
